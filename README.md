# Advanced Gizmos Unity3D Package
#### This package helps you create gizmos for both the editor and the game build.
#### It makes visual debugging easier.

### How does it work?
- Editor Gizmos :
Unity has a built-in class called "**Gizmos**". It works just like "**Handles**" but in a 3D space.
Creating shapes which are only visible in the Editor for the developers to debug the game.
It can be use to show bounds of a zone, display a collider even when unselected and still
hide them when needed.
But they are not visible in the game build as gizmos are not part of the compilation process
and are Editor only.

- Build Gizmos :
Gizmos visible in build doesn't use the "*Gizmos*" class but the "**Graphics**" class. It forces
the engine to draw meshes on screen without an object attached to them. This is the easiest way
to draw gizmos within the built project as using the "**GL**" class is more complex (But still
used to draw lines and shapes from lines, as the Graphics class draws meshes)