﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public enum GizmoType
{
    BOX,
    WIREBOX,
    SPHERE,
    WIRESPHERE
}

public class AG_Gizmo : MonoBehaviour
{
    #region Variables
    [Header("Properties")]
    public GizmoType gizmoType = GizmoType.BOX;
    public Vector3 offset = Vector3.zero;
    public Vector3 scale = Vector3.one;
    public Quaternion rotation = Quaternion.identity;
    public float radius = 1.0f;

    [Header("Color")]
    public Color color = Color.white;
    [Range(0.0f, 1.0f)]
    public float gizmoOpacity = 0.5f;
    
    [Header("Outline")]
    public bool outline = true;   
    [Range(0.0f,1.0f)]
    public float outlineOpacity = 1.0f;
    [Range(0.0f,0.5f)]
    public float outlineWidth = 0.49f;
    #endregion

    #region Graphics Gizmos    
    //Called every frame
    private void Update()
    {
        if (!AG_GraphicDraw.active) { return; }
        
        switch (gizmoType)
        {
            case GizmoType.BOX:
                AG_GraphicDraw.Box(transform.position + offset, scale, color, gizmoOpacity, outline, outlineOpacity, outlineWidth);
                break;
            case GizmoType.WIREBOX:
                outlineOpacity = gizmoOpacity;
                AG_GraphicDraw.WireBox(transform.position + offset, scale, color, gizmoOpacity, outlineWidth);
                break;
            case GizmoType.SPHERE:
                AG_GraphicDraw.Box(transform.position + offset, scale, color, gizmoOpacity, outline, outlineOpacity, outlineWidth);
                break;
            case GizmoType.WIRESPHERE:
                outlineOpacity = gizmoOpacity;
                AG_GraphicDraw.WireBox(transform.position + offset, scale, color, gizmoOpacity, outlineWidth);
                break;
        }
    }
    #endregion

    #region Editor Gizmos
    //Only in editor, called every frame
#if UNITY_EDITOR
    [ExecuteInEditMode]
    private void OnDrawGizmos()
    {
        if(!AG_GizmoDraw.active || EditorApplication.isPlaying) { return; }

        switch (gizmoType)
        {
            case GizmoType.BOX:
                AG_GizmoDraw.Box(transform.position + offset, scale, color, gizmoOpacity, outline, outlineOpacity);
                break;
            case GizmoType.WIREBOX:
                outlineOpacity = gizmoOpacity;
                AG_GizmoDraw.WireBox(transform.position + offset, scale, color, gizmoOpacity);
                break;
            case GizmoType.SPHERE:
                AG_GizmoDraw.Box(transform.position + offset, scale, color, gizmoOpacity, outline, outlineOpacity);
                break;
            case GizmoType.WIRESPHERE:
                outlineOpacity = gizmoOpacity;
                AG_GizmoDraw.WireBox(transform.position + offset, scale, color, gizmoOpacity);
                break;
        }
    }
#endif
    #endregion
}
