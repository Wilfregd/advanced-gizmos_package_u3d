﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AG_GraphicDraw
{
    public static bool active = true;

    private static Mesh ExtractMesh(GameObject obj)
    {
        Mesh m = obj.GetComponent<MeshFilter>().sharedMesh;
        MonoBehaviour.DestroyImmediate(obj);
        return m;
    }

    public static void Box(Vector3 position, Vector3 scale, Color color, float gizmoOpacity = 0.5f, bool outline = false, float outlineOpacity = 1, float outlineWidth = 0.4f)
    {
        Material mat = new Material(Shader.Find("Custom/Wireframe"));        
        Color _col = color;
        _col.a = gizmoOpacity;
        mat.SetFloat("_Boundry", outlineWidth);
        mat.SetColor("_Color", _col); //Fillcolor

        if (outline)
        {
            _col.a = outlineOpacity;
            mat.SetColor("_Color1", _col); //Wirecolor
        }
        else
        {
            _col.a = 0x00;
            mat.SetColor("_Color1", _col);
        }
        Matrix4x4 matrix = Matrix4x4.TRS(position, Quaternion.identity, scale);
        Graphics.DrawMesh(ExtractMesh(GameObject.CreatePrimitive(PrimitiveType.Cube)), matrix, mat, 0);
    }

    public static void WireBox(Vector3 position, Vector3 scale, Color color, float gizmoOpacity = 0.5f, float outlineWidth = 0.4f)
    {
        Material mat = new Material(Shader.Find("Custom/Wireframe"));
        Color _col = color;
        
        _col.a = 0x00;
        mat.SetFloat("_Boundry", outlineWidth);
        mat.SetColor("_Color", _col); //Fillcolor

        _col.a = gizmoOpacity;
        mat.SetColor("_Color1", _col); //Wirecolor
        
        Matrix4x4 matrix = Matrix4x4.TRS(position, Quaternion.identity, scale);
        Graphics.DrawMesh(ExtractMesh(GameObject.CreatePrimitive(PrimitiveType.Cube)), matrix, mat, 0);
    }
}
