﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AG_GizmoDraw
{
    public static bool active = true;

    public static void Box(Vector3 position, Vector3 scale, Color color, float gizmoOpacity = 0.5f, bool outline = false, float outlineOpacity = 1)
    {
        Color _col = color;
        _col.a = gizmoOpacity;
        Gizmos.color = _col;
        Gizmos.DrawCube(position, scale);

        if(outline)
        {
            _col = color;
            _col.a = outlineOpacity;
            Gizmos.color = _col;
            Gizmos.DrawWireCube(position, scale);
        }

        Gizmos.color = Color.white; //Default
    }

    public static void WireBox(Vector3 position, Vector3 scale, Color color, float gizmoOpacity = 0.5f)
    {
        Color _col = color;
        _col.a = gizmoOpacity;
        Gizmos.color = _col;
        Gizmos.DrawWireCube(position, scale);
        Gizmos.color = Color.white; //Default
    }
}
