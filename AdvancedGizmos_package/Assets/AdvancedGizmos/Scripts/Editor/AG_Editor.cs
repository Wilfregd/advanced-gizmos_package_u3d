﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AG_Gizmo))]
public class AG_Editor : Editor
{
    public override void OnInspectorGUI()
    {
        AG_Gizmo gizmo = (AG_Gizmo)target;

        gizmo.gizmoType = (GizmoType)EditorGUILayout.EnumPopup("Gizmo type", gizmo.gizmoType);

        GUILayout.Space(10.0f);
        
        GUIStyle title = new GUIStyle();
        title.normal.textColor = Color.white;
        title.fontSize = 15;
        title.fontStyle = FontStyle.Bold;
        title.alignment = TextAnchor.MiddleCenter;

        switch (gizmo.gizmoType)
        {
            case GizmoType.BOX:
                #region BOX
                GUILayout.Label("Properties", title);

                gizmo.offset = EditorGUILayout.Vector3Field("Offset", gizmo.offset);
                gizmo.scale = EditorGUILayout.Vector3Field("Scale", gizmo.scale);

                GUILayout.Space(5.0f);
                gizmo.color = EditorGUILayout.ColorField("Color", gizmo.color);
                GUILayout.Space(5.0f);

                GUILayout.BeginHorizontal();
                GUILayout.Label("Gizmo Opacity");
                gizmo.gizmoOpacity = EditorGUILayout.Slider(gizmo.gizmoOpacity, 0.0f, 1.0f);
                GUILayout.EndHorizontal();

                GUILayout.Space(5.0f);

                GUILayout.Label("Outline", title);

                GUILayout.BeginHorizontal();
                GUILayout.Label("Show Outline");
                gizmo.outline = EditorGUILayout.Toggle(gizmo.outline);
                GUILayout.EndHorizontal();

                GUILayout.Space(5.0f);

                GUILayout.BeginHorizontal();
                GUILayout.Label("Outline Opacity");
                gizmo.outlineOpacity = EditorGUILayout.Slider(gizmo.outlineOpacity, 0.0f, 1.0f);
                GUILayout.EndHorizontal();

                GUILayout.Space(5.0f);
                
                GUILayout.BeginHorizontal();
                GUILayout.Label("Outline Width");
                gizmo.outlineWidth = EditorGUILayout.Slider(gizmo.outlineWidth, 0.0f, 0.5f);
                GUILayout.EndHorizontal();

                SceneView.RepaintAll(); //Realtime editing in scene view
                #endregion
                break;
            case GizmoType.WIREBOX:
                #region WIREBBOX
                GUILayout.Label("Properties", title);

                gizmo.offset = EditorGUILayout.Vector3Field("Offset", gizmo.offset);
                gizmo.scale = EditorGUILayout.Vector3Field("Scale", gizmo.scale);

                GUILayout.Space(5.0f);
                gizmo.color = EditorGUILayout.ColorField("Color", gizmo.color);
                GUILayout.Space(5.0f);

                GUILayout.BeginHorizontal();
                GUILayout.Label("Gizmo Opacity");
                gizmo.gizmoOpacity = EditorGUILayout.Slider(gizmo.gizmoOpacity, 0.0f, 1.0f);
                GUILayout.EndHorizontal();

                GUILayout.Space(5.0f);

                GUILayout.Label("Outline", title);
                GUILayout.Space(5.0f);
                GUILayout.BeginHorizontal();
                GUILayout.Label("Outline Width");
                gizmo.outlineWidth = EditorGUILayout.Slider(gizmo.outlineWidth, 0.0f, 0.5f);
                GUILayout.EndHorizontal();

                SceneView.RepaintAll(); //Realtime editing in scene view
                #endregion
                break;
        }

        GUILayout.Space(10.0f);
        EditorGUILayout.LabelField("",GUI.skin.horizontalSlider);
        GUILayout.Space(10.0f);

        GUILayout.Label("Gizmos Visibility", title);
        GUILayout.Space(10.0f);

        //Buttons
        GUILayout.BeginHorizontal();
        if(GUILayout.Button("Show"))
        {
            //Force unfold
            AG_Gizmo[] components = Resources.FindObjectsOfTypeAll<AG_Gizmo>();

            foreach (AG_Gizmo g in components)
                UnityEditorInternal.InternalEditorUtility.SetIsInspectorExpanded(g, true);

            AG_GizmoDraw.active = true;
            AG_GraphicDraw.active = true;
            SceneView.RepaintAll();
        }
        if (GUILayout.Button("Hide"))
        {
            AG_GizmoDraw.active = false;
            AG_GraphicDraw.active = false;
            SceneView.RepaintAll();
        }
        if (GUILayout.Button("Toggle"))
        {
            AG_GizmoDraw.active = !AG_GizmoDraw.active;
            AG_GraphicDraw.active = !AG_GraphicDraw.active;
            SceneView.RepaintAll();
        }
        GUILayout.EndHorizontal();
        GUILayout.Space(20.0f);
    }
}
