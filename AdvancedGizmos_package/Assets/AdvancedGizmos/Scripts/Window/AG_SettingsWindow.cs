﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AG_SettingsWindow : EditorWindow
{
    #region Variables
    public static AG_SettingsWindow instance;
    #endregion

    #region Menus
    [MenuItem("Window/Gizmos settings")]
    public static void OpenWindow()
    {
        InitWindow();   
    }
    #endregion

    #region Main Methods
    public static void InitWindow()
    {
        instance = (AG_SettingsWindow)GetWindow<AG_SettingsWindow>();
        instance.titleContent.text = "Gizmos Settings";
    }

    void OnGUI()
    {
        EditorGUILayout.LabelField("Editor Visibility");
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Show"))
        {
            //Force unfold
            AG_Gizmo[] components = Resources.FindObjectsOfTypeAll<AG_Gizmo>();

            foreach (AG_Gizmo g in components)
                UnityEditorInternal.InternalEditorUtility.SetIsInspectorExpanded(g, true);
            
            AG_GizmoDraw.active = true;
            SceneView.RepaintAll();
        }
        if (GUILayout.Button("Hide"))
        {
            AG_GizmoDraw.active = false;
            SceneView.RepaintAll();
        }
        if (GUILayout.Button("Toggle"))
        {
            AG_GizmoDraw.active = !AG_GizmoDraw.active;
            SceneView.RepaintAll();
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(20.0f);

        EditorGUILayout.LabelField("Build Visibility");
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Show"))
        {
            AG_GraphicDraw.active = true;
            SceneView.RepaintAll();
        }
        if (GUILayout.Button("Hide"))
        {
            AG_GraphicDraw.active = false;
            SceneView.RepaintAll();
        }
        if (GUILayout.Button("Toggle"))
        {
            AG_GraphicDraw.active = !AG_GraphicDraw.active;
            SceneView.RepaintAll();
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(20.0f);
    }
    #endregion
}
